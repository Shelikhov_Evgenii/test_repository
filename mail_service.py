import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.image import MIMEImage
import io
import time

import psycopg2
from apscheduler.schedulers.background import BackgroundScheduler
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.dates as mdates


def send_email(week_image_b, all_image_b):
    msg = MIMEMultipart()
    msg['Subject'] = 'Количество непроанализированных ошибок TVH'
    msg['From'] = 'python_test@mts.ru'
    recipients = ['user_name@mts.ru', 'user_name@mts.ru']
    msg['To'] = ', '.join(recipients)
    body = '''<p>За неделю</p><br>
            <img src="cid:image1"><br>
            <p>За все время</p><br>
            <img src="cid:image2"><br>'''
    msg_text = MIMEText(body, 'html')
    msg.attach(msg_text)
    i = 1
    for image in week_image_b, all_image_b:
        img = MIMEImage(image)
        img.add_header('Content-ID', '<image{}>'.format(i))
        msg.attach(img)
        i += 1

    s = smtplib.SMTP('mail.inside.mts.ru')
    s.send_message(msg)
    s.quit()


def get_data():
    connection = psycopg2.connect(user='username',
                                  password='password',
                                  host='ip_address',
                                  port='port_number',
                                  database='db_name')
    with connection.cursor() as cursor:
        cursor.execute('select * from scheme_name.table_name')
        records = cursor.fetchall()
    if connection is not None:
        connection.close()
    return records


def plot(x_axis, y_axis):
    buf = io.BytesIO()
    plt.figure(figsize=(18, 9))
    plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
    plt.plot(x_axis, y_axis, linewidth=3, color='red')
    plt.xticks(x_axis)
    plt.gcf().autofmt_xdate()
    plt.ylabel('количество ошибок')
    plt.xlabel('дни')
    plt.grid(b=True, which='major', color='#666666', linestyle='-')
    plt.minorticks_on()
    plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
    plt.savefig(buf, format='png', bbox_inches='tight')
    buf.seek(0)
    plt.clf()
    plt.close()
    return buf.read()


def main():
    print('started')
    data = get_data()
    errors, dates = list(), list()
    for row in data:
        dates.append(row[0].date())
        errors.append(row[1])
    all_image_b = plot(dates, errors)
    week_image_b = plot(dates[-7:], errors[-7:])
    send_email(week_image_b, all_image_b)
    scheduler.print_jobs()


scheduler = BackgroundScheduler()
scheduler.add_job(main, 'cron', id='history_analyse_job', hour=16)
scheduler.start()
scheduler.print_jobs()
try:
    # This is here to simulate application activity (which keeps the main thread alive).
    while True:
        time.sleep(2)
except (KeyboardInterrupt, SystemExit):
    # Not strictly necessary if daemonic mode is enabled but should be done if possible
    scheduler.shutdown()
