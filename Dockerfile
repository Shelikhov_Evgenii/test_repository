FROM python:latest
COPY . /app
WORKDIR /app
#RUN pip3 --trusted-host nexus.bss.ural.mts.ru install -i https://nexus.bss.ural.mts.ru/repository/pypi-proxy/simple -r requirements.txt
ENTRYPOINT ["python", "-u", "mail-service.py"]
